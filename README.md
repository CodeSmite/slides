## Waiter Android app ##

This is a work in progress smartphone application I developed a few years ago.  
Working on upgrading, refactoring, unit-testing, testing and redesigning.


## Compatibility ##

Android 4.4+  
UI has currently only been tested on 5 inch screens.