package com.example.chris.slides.ui;

import com.example.chris.slides.BuildConfig;
import com.example.chris.slides.dao.DataBaseHelper;
import com.example.chris.slides.ui.activities.MainActivity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.util.ActivityController;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21, packageName = "com.example.chris.slides")
public class MainActivityTest {

    @Mock
    protected DataBaseHelper mockDataBaseHelper;

    private MainActivity mainActivity;

    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);

        ActivityController<MainActivity> activityController = Robolectric.buildActivity(MainActivity.class);
        Robolectric.getBackgroundThreadScheduler().pause();

        mainActivity = activityController.get();
        mainActivity.isTestMode = true;
        mainActivity.setDataBaseHelper(mockDataBaseHelper);
        mainActivity = activityController.setup().get();
    }

    @Test
    public void onCreate_shouldDataBaseHelperCreateDataBase_whenMainActivityIsCreated() throws Exception {
        Robolectric.getBackgroundThreadScheduler().unPause();

        verify(mockDataBaseHelper, times(1)).createDataBase();
    }

    @Test
    public void onCreate_shouldDataBaseHelperOpenDataBase_whenMainActivityIsCreated() throws Exception {
        Robolectric.getBackgroundThreadScheduler().unPause();

        verify(mockDataBaseHelper, times(1)).openDataBase();
    }

}
