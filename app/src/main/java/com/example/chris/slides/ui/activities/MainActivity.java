package com.example.chris.slides.ui.activities;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.View;

import com.example.chris.slides.R;
import com.example.chris.slides.dao.DataBaseHelper;
import com.example.chris.slides.dao.DataBaseHelperImpl;
import com.example.chris.slides.ui.fragments.FragmentMenu;
import com.example.chris.slides.ui.fragments.FragmentOrders;
import com.example.chris.slides.ui.fragments.FragmentUnpaid;

import java.io.IOException;
import java.util.Locale;


public class MainActivity extends FragmentActivity implements ActionBar.TabListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
     * will keep every loaded fragment in memory. If this becomes too memory
     * intensive, it may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter sectionsPagerAdapter;
    private DataBaseHelper dataBaseHelper;
    private ViewPager viewPager;

    public boolean isTestMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUpDatabase();

        if (!isTestMode)
            setUpViewPager();
    }

    public void openSecondActivity(View view) {
        Intent intent = new Intent(this, AvailableTablesActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    public void setDataBaseHelper(DataBaseHelper dataBaseHelper) {
        this.dataBaseHelper = dataBaseHelper;
    }

    private void setUpDatabase() {
        if (!isTestMode)
            dataBaseHelper = new DataBaseHelperImpl(this);

        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            throw new Error("Unable to create database", e);
        }

        try {
            dataBaseHelper.openDataBase();
        } catch (SQLException e) {
            throw new SQLException("Unable to open database", e);
        }
    }


    private void setUpViewPager() {
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the app.
        sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(sectionsPagerAdapter);
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        public SectionsPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            Fragment fragment = new Fragment();
            switch (position) {
                case 0:
                    return new FragmentMenu();
                case 1:
                    return new FragmentOrders();
                case 2:
                    return new FragmentUnpaid();
                default:
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale locale = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_section1).toUpperCase(locale);
                case 1:
                    return getString(R.string.title_section2).toUpperCase(locale);
                case 2:
                    return getString(R.string.title_section3).toUpperCase(locale);
            }
            return null;
        }

    }

}