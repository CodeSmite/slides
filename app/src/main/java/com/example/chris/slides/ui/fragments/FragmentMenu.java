package com.example.chris.slides.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.Toast;

import com.example.chris.slides.R;
import com.example.chris.slides.adapters.ExpandableListAdapter;
import com.example.chris.slides.dao.DataBaseHelper;
import com.example.chris.slides.dao.DataBaseHelperImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class FragmentMenu extends Fragment {

    private ExpandableListAdapter listAdapter;
    private ExpandableListView expListView;
    private List<String> listDataHeader;
    private HashMap<String, List<String>> listDataChild;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_menu, container, false);

        // get the listview
        expListView = (ExpandableListView) rootView.findViewById(R.id.menu_items);

        // preparing list data
        prepareListData();

        listAdapter = new ExpandableListAdapter(getActivity().getApplicationContext(), listDataHeader, listDataChild);

        // setting list adapter
        expListView.setAdapter(listAdapter);

        // Listview Group click listener
        expListView.setOnGroupClickListener(new OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                return false;
            }
        });

        // Listview Group expanded listener
        expListView.setOnGroupExpandListener(new OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                Toast.makeText(getActivity().getApplicationContext(),
                        listDataHeader.get(groupPosition) + " Expanded",
                        Toast.LENGTH_SHORT).show();
            }
        });

        // Listview Group collasped listener
        expListView.setOnGroupCollapseListener(new OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                Toast.makeText(getActivity().getApplicationContext(),
                        listDataHeader.get(groupPosition) + " Collapsed",
                        Toast.LENGTH_SHORT).show();

            }
        });

        // Listview on child click listener
        expListView.setOnChildClickListener(new OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub
                Toast.makeText(
                        getActivity().getApplicationContext(),
                        listDataHeader.get(groupPosition)
                                + " : "
                                + listDataChild.get(
                                listDataHeader.get(groupPosition)).get(
                                childPosition), Toast.LENGTH_SHORT)
                        .show();
                return false;
            }
        });

        return rootView;
    }

    /*
     * Preparing the list data
     */
    private void prepareListData() {
        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap<>();

        DataBaseHelper db = new DataBaseHelperImpl(getActivity());

        // Adding child data
        listDataHeader.add("Main Dishes");
        listDataHeader.add("Side Dishes");
        listDataHeader.add("Desserts");
        listDataHeader.add("Drinks");

        // Adding child data
        List<String> main_dishes = new ArrayList<>();
        main_dishes.addAll(db.getListRecords("select (item_name||'           €'||item_price) as item_desc from item\n" +
                "join category on item_category_fk = category._id\n" +
                "where category_main_category_fk = '1'\n" +
                "order by item_name", "item_desc"));


        List<String> side_dishes = new ArrayList<>();
        side_dishes.addAll(db.getListRecords("select (item_name||'           €'||item_price) as item_desc from item\n" +
                "join category on item_category_fk = category._id\n" +
                "where category_main_category_fk = '2'\n" +
                "order by item_name", "item_desc"));



        List<String> desserts = new ArrayList<>();
        desserts.addAll(db.getListRecords("select (item_name||'           €'||item_price) as item_desc from item\n" +
                "join category on item_category_fk = category._id\n" +
                "where category_main_category_fk = '3'\n" +
                "order by item_name", "item_desc"));


        List<String> drinks = new ArrayList<>();
        drinks.addAll(db.getListRecords("select (item_name||'           €'||item_price) as item_desc from item\n" +
                "join category on item_category_fk = category._id\n" +
                "where category_main_category_fk = '4'\n" +
                "order by item_name", "item_desc"));


        listDataChild.put(listDataHeader.get(0), main_dishes); // Header, Child data
        listDataChild.put(listDataHeader.get(1), side_dishes);
        listDataChild.put(listDataHeader.get(2), desserts);
        listDataChild.put(listDataHeader.get(3), drinks);
    }

}
