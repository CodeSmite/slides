package com.example.chris.slides.ui.activities;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;

import com.example.chris.slides.R;
import com.example.chris.slides.ui.fragments.FragmentItemsSelected;
import com.example.chris.slides.ui.fragments.FragmentOrderNotes;
import com.example.chris.slides.ui.fragments.FragmentSelectDessert;
import com.example.chris.slides.ui.fragments.FragmentSelectDrink;
import com.example.chris.slides.ui.fragments.FragmentSelectMainDish;
import com.example.chris.slides.ui.fragments.FragmentSelectSideDish;

import java.util.Locale;


public class AvailableTablesActivity extends FragmentActivity implements ActionBar.TabListener, FragmentSelectMainDish.OnMenuItemSelectedListener {
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
     * will keep every loaded fragment in memory. If this becomes too memory
     * intensive, it may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link android.support.v4.view.ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_available_tables);

        getActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            getActionBar().setTitle("Orders for " + extras.getString("table_name"));
        }

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the app.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());


        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager_available_tables);
        mViewPager.setOffscreenPageLimit(1);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        //Find if user is at page 1 to call onResume() to refresh the view
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            public void onPageSelected(int position) {
                // Check if this is the page you want.
                if (position == 0) {
                    FragmentItemsSelected fragment = (FragmentItemsSelected) mSectionsPagerAdapter.getRegisteredFragment(0);

                    if (fragment != null) {
                        fragment.onResume();
                    }
                }
            }
        });

    }

    //fragment interface implementation
    //test
    @Override
    public void onItemSelected(int position) {
        FragmentItemsSelected fragment = (FragmentItemsSelected) mSectionsPagerAdapter.getRegisteredFragment(0);

        if (fragment != null) {
            fragment.onResume();
        }
    }


    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        SparseArray<Fragment> registeredFragments = new SparseArray<>();

        public SectionsPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            Fragment fragment = new Fragment();
            switch (position) {
                case 0:
                    return new FragmentItemsSelected();
                case 1:
                    return new FragmentSelectMainDish();
                case 2:
                    return new FragmentSelectSideDish();
                case 3:
                    return new FragmentSelectDessert();
                case 4:
                    return new FragmentSelectDrink();
                case 5:
                    return new FragmentOrderNotes();
                default:
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            // Show 6 total pages.
            return 6;
        }

        //Name tabs
        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_available_section1).toUpperCase(l);
                case 1:
                    return getString(R.string.title_available_section2).toUpperCase(l);
                case 2:
                    return getString(R.string.title_available_section3).toUpperCase(l);
                case 3:
                    return getString(R.string.title_available_section4).toUpperCase(l);
                case 4:
                    return getString(R.string.title_available_section5).toUpperCase(l);
                case 5:
                    return getString(R.string.title_available_section6).toUpperCase(l);
            }
            return null;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            //save instantiated fragments
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            registeredFragments.put(position, fragment);
            return fragment;
        }

        //return the current fragment
        public Fragment getRegisteredFragment(int position) {
            return registeredFragments.get(position);
        }

    }
}
