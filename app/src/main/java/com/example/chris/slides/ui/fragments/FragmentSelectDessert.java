package com.example.chris.slides.ui.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.chris.slides.R;
import com.example.chris.slides.adapters.MenuItemPriceCustomAdapter;
import com.example.chris.slides.dao.DataBaseHelperImpl;
import com.example.chris.slides.models.Item;
import com.example.chris.slides.models.MenuItemPrice;
import com.example.chris.slides.models.SectionMenuCategory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class FragmentSelectDessert extends Fragment {


    public FragmentSelectDessert() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ListView userList;
        MenuItemPriceCustomAdapter itemAdapter;
        ArrayList<Item> itemArray = new ArrayList<Item>();

        View rootView = inflater.inflate(R.layout.select_main_dish, container, false);

        /**
         * add item in arraylist
         */
        DataBaseHelperImpl db = new DataBaseHelperImpl(getActivity());

        ArrayList<HashMap<String, String>> map_list = new ArrayList<HashMap<String, String>>();


        map_list.addAll(db.getListHashRecords("select item._id as item_id, item.item_name, item.item_price, item.item_configure, category._id as category_id, category.category_name from item\n" +
                "join category on item_category_fk = category._id\n" +
                "join main_category on category.category_main_category_fk = main_category._id\n" +
                "where main_category._id = 3\n" +
                "order by category._id"));

        db.close();

        String item_id = "";
        String item_name = "";
        String item_price = "";
        String item_configure = "";
        String category_id = "";
        String prv_category_id = "";
        String category_name = "";


        for (HashMap<String, String> map : map_list) {
            for (Map.Entry<String, String> entry : map.entrySet()) {

                if (entry.getKey().equals("category_id")) {
                    prv_category_id = category_id;
                    category_id = entry.getValue();
                }

                if (entry.getKey().equals("category_name")) {
                    category_name = entry.getValue();
                }

                if (entry.getKey().equals("item_name")) {
                    item_name = entry.getValue();
                }

                if (entry.getKey().equals("item_price")) {
                    item_price = entry.getValue();
                }

                if (entry.getKey().equals("item_id")) {
                    item_id = entry.getValue();
                }

                if (entry.getKey().equals("item_configure")) {
                    item_configure = entry.getValue();
                }
            }

            if (!(prv_category_id.equals(category_id))) {
                itemArray.add(new SectionMenuCategory(category_name, category_id));
            }

            itemArray.add(new MenuItemPrice(item_id, item_name, item_price, item_configure, category_id));

        }
        /*
        itemArray.add(new SectionMenuCategory("Cakes","1"));
        itemArray.add(new MenuItemPrice("Cheese Cake", "£3.00","2"));
        itemArray.add(new MenuItemPrice("Chocolate", "£3.00","2"));
        itemArray.add(new MenuItemPrice("Apple", "$3.00","2"));
        itemArray.add(new MenuItemPrice("Lime", "$3.00","2"));
        itemArray.add(new SectionMenuCategory("Fruit","1"));
        itemArray.add(new MenuItemPrice("Banana", "$1.00","2"));
        itemArray.add(new MenuItemPrice("Apple", "$1.20","2"));
        itemArray.add(new MenuItemPrice("Orange", "$1.00","2"));
        itemArray.add(new MenuItemPrice("Pineapple", "$3.00","2"));
        */
        /**
         * set item into adapter
         */
        itemAdapter = new MenuItemPriceCustomAdapter(getActivity(), R.layout.menu_row,
                itemArray);
        userList = (ListView) rootView.findViewById(R.id.listView);
        userList.setItemsCanFocus(false);


        userList.setAdapter(itemAdapter);
        userList.setBackgroundColor(Color.WHITE);


        return rootView;
    }

}

