package com.example.chris.slides.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.chris.slides.R;
import com.example.chris.slides.dao.DataBaseHelperImpl;


public class FragmentOrderNotes extends Fragment {

    public static final String ARG_SECTION_NUMBER = "section_number";

    private ImageButton saveButton;
    private ImageButton clearButton;
    private EditText notesTextView;

    private String table_id = "";
    private String order_id = "";


    public FragmentOrderNotes() {

    }


    @Override
    public void onResume() {
        super.onResume();


        String actionBarTitle = (String) getActivity().getActionBar().getTitle();

        String tableName = actionBarTitle.replace("Orders for ", "");

        if (table_id.isEmpty()) {
            DataBaseHelperImpl db = new DataBaseHelperImpl(getActivity());

            table_id = db.getRecord("select _id as tables_id from tables\n" +
                    "where tables_name = '" + tableName + "'", "tables_id");

            db.close();
        }


        if (order_id.isEmpty()) {
            DataBaseHelperImpl db = new DataBaseHelperImpl(getActivity());

            order_id = db.getRecord("select orders._id as orders_id from orders\n" +
                    "where orders.orders_tables_fk = '" + table_id + "' and orders.orders_status_fk = '5'", "orders_id");

            db.close();
        }


        if (order_id.isEmpty()) {
            saveButton.setEnabled(false);
        } else {
            saveButton.setEnabled(true);

            DataBaseHelperImpl db = new DataBaseHelperImpl(getActivity());

            String notes = db.getRecord("SELECT orders_notes from orders\n" +
                    "WHERE orders._id = '" + order_id + "'", "orders_notes");

            db.close();

            notesTextView.setText(notes);
        }


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_order_notes, container, false);
        notesTextView = (EditText) rootView.findViewById(R.id.notes);

        saveButton = (ImageButton) rootView.findViewById(R.id.saveButton);

        saveButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                DataBaseHelperImpl db = new DataBaseHelperImpl(getActivity());

                db.updateRecords("orders", "orders_notes", notesTextView.getText().toString(), order_id);

                db.close();

                Toast.makeText(getActivity(),
                        "Saved", Toast.LENGTH_SHORT).show();
            }
        });


        clearButton = (ImageButton) rootView.findViewById(R.id.clearNotesButton);

        clearButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                notesTextView.setText("");
            }
        });

        return rootView;
    }
}
