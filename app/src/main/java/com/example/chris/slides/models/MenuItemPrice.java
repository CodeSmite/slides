package com.example.chris.slides.models;


public class MenuItemPrice implements Item {

    private String itemId;
    private String name;
    private String price;
    private String config;
    private String categoryId;

    public MenuItemPrice(String itemId, String name, String price, String config, String categoryId) {
        super();
        this.itemId = itemId;
        this.name = name;
        this.price = price;
        this.config = config;
        this.categoryId = categoryId;
    }


    public String getConfig() {
        return config;
    }

    public String getItemId() {
        return itemId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getPrice() {
        return price;
    }

    @Override
    public boolean isSection() {
        return false;
    }
}
