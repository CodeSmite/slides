package com.example.chris.slides.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class DataBaseHelperImpl extends SQLiteOpenHelper implements DataBaseHelper {

    private static final String DB_PATH = "/data/data/com.example.chris.slides/databases/";

    private static final String DB_NAME = "restaurant.db";

    private SQLiteDatabase myDataBase;

    private final Context myContext;


    public DataBaseHelperImpl(Context context) {
        super(context, DB_NAME, null, 3);
        this.myContext = context;
    }

    public void createDataBase() throws IOException {

        boolean dbExist = checkDataBase();
        //Comment above line and uncomment line bellow to copy new db
        //boolean dbExist = false;

        if (dbExist) {
            //do nothing - database already exist
        } else {
            //By calling this method an empty database will be created into the default system path
            //of your application so we are gonna be able to overwrite that database with our database.
            this.getReadableDatabase();

            try {
                copyDataBase();
            } catch (IOException e) {
                throw new Error("Error copying database");
            }
        }

    }


    private boolean checkDataBase() {
        SQLiteDatabase checkDB = null;

        try {
            String myPath = DB_PATH + DB_NAME;
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);

        } catch (SQLiteException e) {
            System.out.printf("Database doesn't exist " + e);
        }

        if (checkDB != null) {

            checkDB.close();

        }

        return checkDB != null;
    }


    private void copyDataBase() throws IOException {
        //Open your local db as the input stream
        InputStream myInput = myContext.getAssets().open(DB_NAME);
        // Path to the just created empty db
        String outFileName = DB_PATH + DB_NAME;

        try (OutputStream myOutput = new FileOutputStream(outFileName)) {
            //transfer bytes from the inputfile to the outputfile
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer)) > 0) {
                myOutput.write(buffer, 0, length);
            }
        } catch (IOException e) {
            System.out.printf("Failed to read database" + e);
        } finally {
            myInput.close();
        }


    }

    public void openDataBase() throws SQLException {
        //Open the database
        String myPath = DB_PATH + DB_NAME;
        myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
    }

    @Override
    public synchronized void close() {
        if (myDataBase != null)
            myDataBase.close();

        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    //Select data and return in an ArrayList
    public List<String> getListRecords(String query, String column) {
        List<String> arrayList = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            arrayList.add(cursor.getString(cursor.getColumnIndex(column)));
            cursor.moveToNext();
        }

        cursor.close();

        return arrayList;
    }


    public String getRecord(String query, String column) {
        String record = "";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            record = cursor.getString(cursor.getColumnIndex(column));
        }

        cursor.close();

        return record;
    }


    public String getRecords(String query, String column) {
        String record = "";
        String comma = "";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                record = record + comma + cursor.getString(cursor.getColumnIndex(column));
                comma = ", ";
            } while (cursor.moveToNext());
        }

        cursor.close();

        return record;
    }


    public List<HashMap<String, String>> getListHashRecords(String query) {
        List<HashMap<String, String>> hashMapArrayList = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<>();
                for (int i = 0; i < cursor.getColumnCount(); i++) {
                    map.put(cursor.getColumnName(i), cursor.getString(i));
                }

                hashMapArrayList.add(map);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return hashMapArrayList;
    }


    public long insertRecords(String table, List<String> column, List<String> value) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        for (int i = 0; i < value.size(); i++) {
            values.put(column.get(i), value.get(i));
        }

        return db.insert(table, null, values);
    }


    public long updateRecords(String table, List<String> column, List<String> value, String id) {
        SQLiteDatabase db = this.getWritableDatabase();

        String strFilter = "_id=" + id;
        ContentValues values = new ContentValues();

        for (int i = 0; i < value.size(); i++) {
            values.put(column.get(i), value.get(i));
        }

        return db.update(table, values, strFilter, null);
    }


    public long updateRecords(String table, String column, String value, String id) {
        SQLiteDatabase db = this.getWritableDatabase();

        String stringFilter = "_id=" + id;
        ContentValues values = new ContentValues();

        values.put(column, value);

        return db.update(table, values, stringFilter, null);
    }


    public boolean deleteRecord(String table, String id) {
        SQLiteDatabase db = this.getWritableDatabase();

        return db.delete(table, "_id =" + id, null) > 0;
    }


    public boolean deleteModifiers(String table, String id) {
        SQLiteDatabase db = this.getWritableDatabase();

        return db.delete(table, "detail_modifier_detail_fk =" + id, null) > 0;
    }


    public boolean deleteRecords(String table, String id) {
        SQLiteDatabase db = this.getWritableDatabase();

        return db.delete(table, id, null) > 0;
    }


    public void executeRawQuery(String query) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL(query);
    }


}