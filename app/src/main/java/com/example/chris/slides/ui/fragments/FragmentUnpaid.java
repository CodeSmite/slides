package com.example.chris.slides.ui.fragments;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;

import com.example.chris.slides.dao.DataBaseHelperImpl;
import com.example.chris.slides.ui.activities.AvailableTablesActivity;

import java.util.ArrayList;


public class FragmentUnpaid extends Fragment implements View.OnClickListener {

    public static final String ARG_SECTION_NUMBER = "section_number";

    public FragmentUnpaid() {
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /*
        View rootView = inflater.inflate(R.layout.fragment_main_dummy, container, false);
        TextView dummyTextView = (TextView) rootView.findViewById(R.id.section_label);
        //dummyTextView.setText(Integer.toString(getArguments().getInt(ARG_SECTION_NUMBER)));




        dummyTextView.setText("3");
        return rootView;
        */

        ScrollView scrollView;
        scrollView = new ScrollView(getActivity());

        //Get device resolution
        WindowManager wm = (WindowManager) getActivity().getApplication().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;


        //Create buttons
        TableLayout layout = new TableLayout(getActivity());
        layout.setBackgroundColor(0x00000000);
        //layout.setLayoutParams( new TableLayout.LayoutParams(4,5) );
        TableLayout.LayoutParams params = new TableLayout.LayoutParams(
                TableLayout.LayoutParams.WRAP_CONTENT,
                TableLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins((int) (width * 0.05), (int) (width * 0.05), (int) (width * 0.05), (int) (width * 0.02));
        //params.setMargins(left, top, right, bottom);


        //layout.setPadding((int) (width * 0.05), (int) (width * 0.05), (int) (width * 0.05), (int) (width * 0.05));

        int num = 0;

        DataBaseHelperImpl db = new DataBaseHelperImpl(getActivity());

        ArrayList<String> tables = new ArrayList<String>();
        /*
        tables.addAll(db.getListRecords("Select DISTINCT tables_name from tables \n" +
                "join orders on orders_tables_fk = tables._id\n" +
                "where orders_status_fk = 1 \n" +
                "order by tables._id asc ", "tables_name"));
                */
        tables.addAll(db.getListRecords("Select DISTINCT tables_name from tables \n" +
                "where tables_status_fk = 2\n" +
                "order by tables._id asc ", "tables_name"));

        db.close();

        for (int f = 0; f < (tables.size() / 2) + 1; f++) {
            TableRow tr = new TableRow(getActivity());
            for (int c = 1; c <= 2; c++) {
                if (num + 1 <= tables.size()) {
                    FrameLayout frameLayout = new FrameLayout(getActivity());
                    frameLayout.setPadding((int) (width * 0.05), (int) (width * 0.001), (int) (width * 0.05), (int) (width * 0.001));
                    Button tableButton = new Button(getActivity().getApplication());
                    tableButton.setText(tables.get(num));
                    num++;
                    tableButton.setTypeface(null, Typeface.BOLD);
                    //tableButton.setTextColor(0xFF000000);
                    tableButton.setTextColor(0xFF33b5e5);
                    tableButton.setBackgroundColor(0xFFFFFFFF);
                    tableButton.setMaxWidth((int) (width * 0.4));
                    tableButton.setLayoutParams(params);

                    tableButton.setOnClickListener(this);

                    frameLayout.addView(tableButton);
                    tr.setBackgroundColor(0x00000000);
                    tr.setLayoutParams(params);
                    tr.addView(frameLayout, (int) (width * 0.45), (int) (height * 0.1));
                }
            } // for
            layout.addView(tr);
        } // for

        scrollView.addView(layout);


        return scrollView;
    }


    public void onClick(View view) {
        openSecondActivity(view);
    }

    public void openSecondActivity(View view) {
        Intent intent = new Intent(getActivity().getApplication(), AvailableTablesActivity.class);
        Button b = (Button) view;
        String buttonText = b.getText().toString();
        intent.putExtra("table_name", buttonText);
        startActivity(intent);
    }
}
