package com.example.chris.slides.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.chris.slides.R;
import com.example.chris.slides.dao.DataBaseHelper;
import com.example.chris.slides.dao.DataBaseHelperImpl;
import com.example.chris.slides.models.Item;
import com.example.chris.slides.models.MenuItemPrice;
import com.example.chris.slides.models.SectionMenuCategory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class MenuItemPriceCustomAdapter extends ArrayAdapter {
    private Context context;
    private int layoutResourceId;
    private LayoutInflater inflater;
    private List<Item> data = new ArrayList<>();

    private String table_id = "";
    private String order_id = "";

    public MenuItemPriceCustomAdapter(Context context, int resource, List<Item> data) {
        super(context, resource, data);
        this.layoutResourceId = resource;
        this.context = context;
        this.data = data;
        inflater = ((Activity) context).getLayoutInflater();
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        MenuItemPriceHolder holder;

        final Item viewItem = data.get(position);
        if (viewItem != null) {
            if (viewItem.isSection()) {
                SectionMenuCategory si = (SectionMenuCategory) viewItem;
                row = inflater.inflate(R.layout.menu_section_header, null);
                row.setOnClickListener(null);
                row.setOnLongClickListener(null);
                row.setLongClickable(false);
                final TextView sectionView =
                        (TextView) row.findViewById(R.id.section_header);
                sectionView.setText(si.getTitle());
            } else {
                final MenuItemPrice item = (MenuItemPrice) viewItem;
                row = inflater.inflate(layoutResourceId, parent, false);
                holder = new MenuItemPriceHolder();
                holder.textItemName = (TextView) row.findViewById(R.id.textView1);
                holder.textPrice = (TextView) row.findViewById(R.id.textView2);
                holder.btnOption = (Button) row.findViewById(R.id.button_options);
                holder.btnOption.setTextColor(0xFF33b5e5);
                holder.btnAdd = (Button) row.findViewById(R.id.button_add);
                holder.btnAdd.setTextColor(0xFF33b5e5);
                row.setTag(holder);

                if (item.getConfig().equals("1")) {
                    holder.btnOption.setVisibility(View.VISIBLE);
                    holder.btnOption.setEnabled(true);
                } else {
                    holder.btnOption.setVisibility(View.INVISIBLE);
                    holder.btnOption.setEnabled(false);
                }

                if (item.getName() != null) {
                    holder.textItemName.setText(item.getName());
                    holder.btnOption.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub


                            DataBaseHelper dbm = new DataBaseHelperImpl((context));
                            List<String> detailList = new ArrayList<>();
                            detailList.clear();
                            detailList.addAll(dbm.getListRecords("select modifier_name from modifier\n" +
                                    "where modifier.modifier_category_fk = " + item.getCategoryId(), "modifier_name"));

                            final CharSequence[] details = detailList.toArray(new CharSequence[detailList.size()]);
                            final List mSelectedItems = new ArrayList();


                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setTitle("Pick detail");
                            builder.setMultiChoiceItems(details, null,
                                    new DialogInterface.OnMultiChoiceClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which,
                                                            boolean isChecked) {
                                            if (isChecked) {
                                                // If the user checked the item, add it to the selected items
                                                mSelectedItems.add(which);
                                            } else if (mSelectedItems.contains(which)) {
                                                // Else, if the item is already in the array, remove it
                                                mSelectedItems.remove(Integer.valueOf(which));
                                            }
                                        }
                                    }
                            ).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int id) {
                                            // User clicked OK, so save the mSelectedItems results somewhere
                                            // or return them to the component that opened the dialog
                                            String selected_details = "";
                                            String comma = "";

                                            for (int i = 0; i < mSelectedItems.size(); i++) {
                                                selected_details = selected_details + comma + "\"" + details[Integer.valueOf(mSelectedItems.get(i).toString())] + "\"";
                                                comma = ",";
                                            }

                                            Toast.makeText(context, "Item Added",
                                                    Toast.LENGTH_LONG).show();


                                            DataBaseHelperImpl dbi = new DataBaseHelperImpl((context));
                                            List<String> selected_detail = new ArrayList<>();
                                            selected_detail.clear();
                                            selected_detail.addAll(dbi.getListRecords("select modifier._id from modifier\n" +
                                                    "where modifier.modifier_category_fk = " + item.getCategoryId() + " and modifier.modifier_name in (" + selected_details + ")", "_id"));


                                            dbi.close();

                                            // TODO Insert or update database
                                            //Insert or update database
                                            String actionBarTitle = (String) ((Activity) context).getActionBar().getTitle();

                                            String tableName = actionBarTitle.replace("Orders for ", "");

                                            if (table_id.isEmpty()) {
                                                DataBaseHelperImpl db = new DataBaseHelperImpl(context);

                                                table_id = db.getRecord("select _id as tables_id from tables\n" +
                                                        "where tables_name = '" + tableName + "'", "tables_id");

                                                db.close();
                                            }


                                            if (order_id.isEmpty()) {
                                                DataBaseHelperImpl db = new DataBaseHelperImpl(context);

                                                order_id = db.getRecord("select orders._id as orders_id from orders\n" +
                                                        "where orders.orders_tables_fk = '" + table_id + "' and orders.orders_status_fk = '5'", "orders_id");

                                                db.close();
                                            }


                                            if (order_id.isEmpty()) {

                                                List<String> col = new ArrayList<>();
                                                List<String> values = new ArrayList<>();

                                                DataBaseHelperImpl db = new DataBaseHelperImpl(context);

                                                String order_ref = db.getRecord("select orders_ref from orders\n" +
                                                        "ORDER BY orders_ref\n" +
                                                        "DESC LIMIT 1", "orders_ref");

                                                order_ref = order_ref.replace("sin", "");

                                                order_ref = String.valueOf(Long.valueOf(order_ref) + 1);

                                                if (order_ref.length() == 1) {
                                                    order_ref = "sin00000000" + order_ref;
                                                } else if (order_ref.length() == 2) {
                                                    order_ref = "sin0000000" + order_ref;
                                                } else if (order_ref.length() == 3) {
                                                    order_ref = "sin000000" + order_ref;
                                                } else if (order_ref.length() == 4) {
                                                    order_ref = "sin00000" + order_ref;
                                                } else if (order_ref.length() == 5) {
                                                    order_ref = "sin0000" + order_ref;
                                                } else if (order_ref.length() == 6) {
                                                    order_ref = "sin000" + order_ref;
                                                } else if (order_ref.length() == 7) {
                                                    order_ref = "sin00" + order_ref;
                                                } else if (order_ref.length() == 8) {
                                                    order_ref = "sin0" + order_ref;
                                                } else if (order_ref.length() == 9) {
                                                    order_ref = "sin" + order_ref;
                                                } else {
                                                    order_ref = "sin000000001";
                                                }


                                                Calendar c = Calendar.getInstance();

                                                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                                                String formattedDate = df.format(c.getTime());

                                                SimpleDateFormat tf = new SimpleDateFormat("HH:mm");
                                                String formattedTime = tf.format(c.getTime());


                                                col.add("orders_ref");
                                                col.add("orders_date");
                                                col.add("orders_time");
                                                col.add("orders_end_time");
                                                col.add("orders_total");
                                                col.add("orders_discount");
                                                col.add("orders_net");
                                                col.add("orders_status_fk");
                                                col.add("orders_tables_fk");
                                                col.add("orders_company_fk");
                                                col.add("orders_user_fk");
                                                col.add("orders_shift_fk");
                                                col.add("orders_notes");


                                                values.add(order_ref);
                                                values.add(formattedDate);
                                                values.add(formattedTime);
                                                values.add("''");
                                                values.add("0.00");
                                                values.add("0.00");
                                                values.add("0.00");
                                                values.add("5");
                                                values.add(table_id);
                                                values.add("1");
                                                values.add("1");
                                                values.add("1");
                                                values.add("");

                                                db.insertRecords("orders", col, values);

                                                order_id = db.getRecord("select orders._id as orders_id from orders\n" +
                                                        "where orders.orders_tables_fk = '" + table_id + "' and orders.orders_status_fk = '5'", "orders_id");

                                                col.clear();
                                                col.add("detail_orders_fk");
                                                col.add("detail_desc");
                                                col.add("detail_qty");
                                                col.add("detail_qty_served");
                                                col.add("detail_weight");
                                                col.add("detail_price");
                                                col.add("detail_inc_price");
                                                col.add("detail_total");
                                                col.add("detail_net");
                                                col.add("detail_item_fk");
                                                col.add("detail_menutype_fk");
                                                col.add("detail_sent");
                                                col.add("detail_void");

                                                values.clear();
                                                values.add(order_id);
                                                values.add(item.getName());
                                                values.add("1");
                                                values.add("0");
                                                values.add("0");
                                                values.add(item.getPrice());
                                                values.add("0");
                                                values.add(item.getPrice());
                                                values.add(item.getPrice());
                                                values.add(item.getItemId());
                                                values.add("1");
                                                values.add("0");
                                                values.add("0");

                                                db.insertRecords("detail", col, values);


                                                //Insert modifiers
                                                col.clear();
                                                col.add("detail_modifier_detail_fk");
                                                col.add("detail_modifier_modifier_fk");
                                                col.add("detail_modifier_name");
                                                col.add("detail_modifier_price");

                                                String detail_last_id = db.getRecord("select MAX(_id) as detail_id from detail\n" +
                                                        "where detail_orders_fk = '" + order_id + "' and detail_item_fk = '" + item.getItemId() + "'", "detail_id");

                                                for (int i = 0; i < selected_detail.size(); i++) {
                                                    values.clear();
                                                    values.add(detail_last_id);
                                                    values.add(selected_detail.get(i));
                                                    values.add((String) details[Integer.valueOf(mSelectedItems.get(i).toString())]);
                                                    String modifier_price = db.getRecord("select modifier_price from modifier\n" +
                                                            "where _id = '" + selected_detail.get(i) + "'", "modifier_price");
                                                    values.add(modifier_price);

                                                    db.insertRecords("detail_modifier", col, values);
                                                }


                                                db.close();

                                            } else {

                                                DataBaseHelperImpl db = new DataBaseHelperImpl(context);

                                                String detail_id = db.getRecord("select _id as detail_id from detail\n" +
                                                        "where detail_orders_fk = '" + order_id + "' and detail_item_fk = '" + item.getItemId() + "'", "detail_id");

                                                db.close();

                                                if (detail_id.isEmpty()) {
                                                    List<String> col = new ArrayList<>();
                                                    List<String> values = new ArrayList<>();

                                                    col.add("detail_orders_fk");
                                                    col.add("detail_desc");
                                                    col.add("detail_qty");
                                                    col.add("detail_qty_served");
                                                    col.add("detail_weight");
                                                    col.add("detail_price");
                                                    col.add("detail_inc_price");
                                                    col.add("detail_total");
                                                    col.add("detail_net");
                                                    col.add("detail_item_fk");
                                                    col.add("detail_menutype_fk");
                                                    col.add("detail_sent");
                                                    col.add("detail_void");

                                                    values.add(order_id);
                                                    values.add(item.getName());
                                                    values.add("1");
                                                    values.add("0");
                                                    values.add("0");
                                                    values.add(item.getPrice());
                                                    values.add("0");
                                                    values.add(item.getPrice());
                                                    values.add(item.getPrice());
                                                    values.add(item.getItemId());
                                                    values.add("1");
                                                    values.add("0");
                                                    values.add("0");

                                                    DataBaseHelperImpl db_i = new DataBaseHelperImpl((Activity) context);

                                                    db_i.insertRecords("detail", col, values);


                                                    //Insert modifiers
                                                    col.clear();
                                                    col.add("detail_modifier_detail_fk");
                                                    col.add("detail_modifier_modifier_fk");
                                                    col.add("detail_modifier_name");
                                                    col.add("detail_modifier_price");

                                                    String detail_last_id = db_i.getRecord("select MAX(_id) as detail_id from detail\n" +
                                                            "where detail_orders_fk = '" + order_id + "' and detail_item_fk = '" + item.getItemId() + "'", "detail_id");

                                                    for (int i = 0; i < selected_detail.size(); i++) {
                                                        values.clear();
                                                        values.add(detail_last_id);
                                                        values.add(selected_detail.get(i));
                                                        values.add((String) details[Integer.valueOf(mSelectedItems.get(i).toString())]);
                                                        String modifier_price = db_i.getRecord("select modifier_price from modifier\n" +
                                                                "where _id = '" + selected_detail.get(i) + "'", "modifier_price");
                                                        values.add(modifier_price);

                                                        db_i.insertRecords("detail_modifier", col, values);
                                                    }

                                                    db_i.close();
                                                } else {

                                                    DataBaseHelperImpl db_uu = new DataBaseHelperImpl(context);

                                                    String detail_desc = db_uu.getRecord("select detail.detail_desc from detail\n" +
                                                            "where detail._id = '" + detail_id + "'", "detail_desc");

                                                    String modifiers_num = db_uu.getRecord("select detail._id as modifiers from detail_modifier\n" +
                                                            "join detail on detail_modifier.detail_modifier_detail_fk = detail._id\n" +
                                                            "group by detail._id\n" +
                                                            "having detail.detail_desc = '" + detail_desc + "' and detail.detail_orders_fk = '" + order_id + "' and detail_modifier.detail_modifier_name IN (" + selected_details + ") and count(detail_modifier._id) =" + selected_detail.size(), "modifiers");

                                                    db_uu.close();

                                                    if (!modifiers_num.isEmpty()) {

                                                        DataBaseHelperImpl db_up = new DataBaseHelperImpl(context);


                                                        String detail_qty = db_up.getRecord("select detail_qty from detail\n" +
                                                                "where detail._id = '" + modifiers_num + "'", "detail_qty");

                                                        String detail_price = db_up.getRecord("select detail_price from detail\n" +
                                                                "where detail._id = '" + modifiers_num + "'", "detail_price");

                                                        List<String> col = new ArrayList<>();
                                                        List<String> values = new ArrayList<>();

                                                        col.add("detail_qty");
                                                        col.add("detail_total");
                                                        col.add("detail_net");

                                                        String qty = String.valueOf(Integer.valueOf(detail_qty) + 1);
                                                        String total_price = String.valueOf(Float.valueOf(detail_price) * (Integer.valueOf(qty)));

                                                        values.add(qty);
                                                        values.add(total_price);
                                                        values.add(total_price);


                                                        db_up.updateRecords("detail", col, values, modifiers_num);

                                                        db_up.close();
                                                    } else {
                                                        List<String> col = new ArrayList<>();
                                                        List<String> values = new ArrayList<>();

                                                        col.add("detail_orders_fk");
                                                        col.add("detail_desc");
                                                        col.add("detail_qty");
                                                        col.add("detail_qty_served");
                                                        col.add("detail_weight");
                                                        col.add("detail_price");
                                                        col.add("detail_inc_price");
                                                        col.add("detail_total");
                                                        col.add("detail_net");
                                                        col.add("detail_item_fk");
                                                        col.add("detail_menutype_fk");
                                                        col.add("detail_sent");
                                                        col.add("detail_void");

                                                        values.add(order_id);
                                                        values.add(item.getName());
                                                        values.add("1");
                                                        values.add("0");
                                                        values.add("0");
                                                        values.add(item.getPrice());
                                                        values.add("0");
                                                        values.add(item.getPrice());
                                                        values.add(item.getPrice());
                                                        values.add(item.getItemId());
                                                        values.add("1");
                                                        values.add("0");
                                                        values.add("0");

                                                        DataBaseHelperImpl db_up = new DataBaseHelperImpl(context);

                                                        db_up.insertRecords("detail", col, values);


                                                        //Insert modifiers
                                                        col.clear();
                                                        col.add("detail_modifier_detail_fk");
                                                        col.add("detail_modifier_modifier_fk");
                                                        col.add("detail_modifier_name");
                                                        col.add("detail_modifier_price");

                                                        String detail_last_id = db_up.getRecord("select MAX(_id) as detail_id from detail\n" +
                                                                "where detail_orders_fk = '" + order_id + "' and detail_item_fk = '" + item.getItemId() + "'", "detail_id");

                                                        for (int i = 0; i < selected_detail.size(); i++) {
                                                            values.clear();
                                                            values.add(detail_last_id);
                                                            values.add(selected_detail.get(i));
                                                            values.add((String) details[Integer.valueOf(mSelectedItems.get(i).toString())]);
                                                            String modifier_price = db_up.getRecord("select modifier_price from modifier\n" +
                                                                    "where _id = '" + selected_detail.get(i) + "'", "modifier_price");
                                                            values.add(modifier_price);

                                                            db_up.insertRecords("detail_modifier", col, values);
                                                        }

                                                        db_up.close();

                                                    }


                                                }

                                            }


                                        }
                                    })
                                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int id) {

                                        }
                                    });

                            builder.show();

                        }
                    });


                    holder.btnAdd.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub

                            //Add selected item to order
                            Toast.makeText(context, "Item Added",
                                    Toast.LENGTH_LONG).show();

                            String actionBarTitle = (String) ((Activity) context).getActionBar().getTitle();

                            String tableName = actionBarTitle.replace("Orders for ", "");

                            if (table_id.isEmpty()) {
                                DataBaseHelperImpl db = new DataBaseHelperImpl(context);

                                table_id = db.getRecord("select _id as tables_id from tables\n" +
                                        "where tables_name = '" + tableName + "'", "tables_id");

                                db.close();
                            }


                            if (order_id.isEmpty()) {
                                DataBaseHelperImpl db = new DataBaseHelperImpl(context);

                                order_id = db.getRecord("select orders._id as orders_id from orders\n" +
                                        "where orders.orders_tables_fk = '" + table_id + "' and orders.orders_status_fk = '5'", "orders_id");

                                db.close();
                            }


                            if (order_id.isEmpty()) {

                                List<String> col = new ArrayList<>();
                                List<String> values = new ArrayList<>();

                                DataBaseHelperImpl db = new DataBaseHelperImpl(context);

                                String order_ref = db.getRecord("select orders_ref from orders\n" +
                                        "ORDER BY orders_ref\n" +
                                        "DESC LIMIT 1", "orders_ref");

                                order_ref = order_ref.replace("sin", "");

                                order_ref = String.valueOf(Long.valueOf(order_ref) + 1);

                                if (order_ref.length() == 1) {
                                    order_ref = "sin00000000" + order_ref;
                                } else if (order_ref.length() == 2) {
                                    order_ref = "sin0000000" + order_ref;
                                } else if (order_ref.length() == 3) {
                                    order_ref = "sin000000" + order_ref;
                                } else if (order_ref.length() == 4) {
                                    order_ref = "sin00000" + order_ref;
                                } else if (order_ref.length() == 5) {
                                    order_ref = "sin0000" + order_ref;
                                } else if (order_ref.length() == 6) {
                                    order_ref = "sin000" + order_ref;
                                } else if (order_ref.length() == 7) {
                                    order_ref = "sin00" + order_ref;
                                } else if (order_ref.length() == 8) {
                                    order_ref = "sin0" + order_ref;
                                } else if (order_ref.length() == 9) {
                                    order_ref = "sin" + order_ref;
                                } else {
                                    order_ref = "sin000000001";
                                }


                                Calendar c = Calendar.getInstance();

                                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                                String formattedDate = df.format(c.getTime());

                                SimpleDateFormat tf = new SimpleDateFormat("HH:mm");
                                String formattedTime = tf.format(c.getTime());


                                col.add("orders_ref");
                                col.add("orders_date");
                                col.add("orders_time");
                                col.add("orders_end_time");
                                col.add("orders_total");
                                col.add("orders_discount");
                                col.add("orders_net");
                                col.add("orders_status_fk");
                                col.add("orders_tables_fk");
                                col.add("orders_company_fk");
                                col.add("orders_user_fk");
                                col.add("orders_shift_fk");
                                col.add("orders_notes");


                                values.add(order_ref);
                                values.add(formattedDate);
                                values.add(formattedTime);
                                values.add("''");
                                values.add("0.00");
                                values.add("0.00");
                                values.add("0.00");
                                values.add("5");
                                values.add(table_id);
                                values.add("1");
                                values.add("1");
                                values.add("1");
                                values.add("");

                                db.insertRecords("orders", col, values);

                                order_id = db.getRecord("select orders._id as orders_id from orders\n" +
                                        "where orders.orders_tables_fk = '" + table_id + "' and orders.orders_status_fk = '5'", "orders_id");

                                col.clear();
                                col.add("detail_orders_fk");
                                col.add("detail_desc");
                                col.add("detail_qty");
                                col.add("detail_qty_served");
                                col.add("detail_weight");
                                col.add("detail_price");
                                col.add("detail_inc_price");
                                col.add("detail_total");
                                col.add("detail_net");
                                col.add("detail_item_fk");
                                col.add("detail_menutype_fk");
                                col.add("detail_sent");
                                col.add("detail_void");

                                values.clear();
                                values.add(order_id);
                                values.add(item.getName());
                                values.add("1");
                                values.add("0");
                                values.add("0");
                                values.add(item.getPrice());
                                values.add("0");
                                values.add(item.getPrice());
                                values.add(item.getPrice());
                                values.add(item.getItemId());
                                values.add("1");
                                values.add("0");
                                values.add("0");

                                db.insertRecords("detail", col, values);

                                db.close();

                            } else {

                                DataBaseHelperImpl db = new DataBaseHelperImpl(context);

                                String detailId = db.getRecord("select _id as detail_id from detail\n" +
                                        "where detail_orders_fk = '" + order_id + "' and detail_item_fk = '" + item.getItemId() + "'", "detail_id");

                                db.close();

                                if (detailId.isEmpty()) {
                                    List<String> col = new ArrayList<>();
                                    List<String> values = new ArrayList<>();

                                    col.add("detail_orders_fk");
                                    col.add("detail_desc");
                                    col.add("detail_qty");
                                    col.add("detail_qty_served");
                                    col.add("detail_weight");
                                    col.add("detail_price");
                                    col.add("detail_inc_price");
                                    col.add("detail_total");
                                    col.add("detail_net");
                                    col.add("detail_item_fk");
                                    col.add("detail_menutype_fk");
                                    col.add("detail_sent");
                                    col.add("detail_void");

                                    values.add(order_id);
                                    values.add(item.getName());
                                    values.add("1");
                                    values.add("0");
                                    values.add("0");
                                    values.add(item.getPrice());
                                    values.add("0");
                                    values.add(item.getPrice());
                                    values.add(item.getPrice());
                                    values.add(item.getItemId());
                                    values.add("1");
                                    values.add("0");
                                    values.add("0");

                                    DataBaseHelperImpl db_i = new DataBaseHelperImpl(context);

                                    db_i.insertRecords("detail", col, values);

                                    db_i.close();
                                } else {

                                    DataBaseHelperImpl db_u = new DataBaseHelperImpl(context);

                                    String detail_qty = db_u.getRecord("select detail_qty from detail\n" +
                                            "where detail._id = '" + detailId + "'", "detail_qty");

                                    String detail_price = db_u.getRecord("select detail_price from detail\n" +
                                            "where detail._id = '" + detailId + "'", "detail_price");

                                    List<String> col = new ArrayList<>();
                                    List<String> values = new ArrayList<>();

                                    col.add("detail_qty");
                                    col.add("detail_total");
                                    col.add("detail_net");

                                    String quantity = String.valueOf(Integer.valueOf(detail_qty) + 1);
                                    String totalPrice = String.valueOf(Float.valueOf(detail_price) * (Integer.valueOf(quantity)));

                                    values.add(quantity);
                                    values.add(totalPrice);
                                    values.add(totalPrice);


                                    db_u.updateRecords("detail", col, values, detailId);

                                    db_u.close();

                                }

                            }


                        }
                    });
                }
                if (item.getPrice() != null) {
                    holder.textPrice.setText("€" + String.format("%.02f", Float.valueOf(item.getPrice())));
                }
            }
        }

        return row;

    }

    private static class MenuItemPriceHolder {
        TextView textItemName;
        TextView textPrice;
        Button btnOption;
        Button btnAdd;
    }
}
