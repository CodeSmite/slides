package com.example.chris.slides.models;


public class SectionMenuCategory implements Item {

    private String id;
    private String name;

    public SectionMenuCategory(String name, String id) {
        super();
        this.name = name;
        this.id = id;
    }


    public String getTitle() {
        return name;
    }

    public void setTitle(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean isSection() {
        return true;
    }
}
