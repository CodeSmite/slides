package com.example.chris.slides.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.chris.slides.R;
import com.example.chris.slides.dao.DataBaseHelperImpl;
import com.example.chris.slides.models.MenuItem;
import com.example.chris.slides.ui.fragments.FragmentItemsSelected;

import java.util.ArrayList;
import java.util.List;


public class MenuItemCustomAdapter extends ArrayAdapter {
    private Context context;
    private int layoutResourceId;
    private List<MenuItem> data = new ArrayList<>();

    public MenuItemCustomAdapter(Context context, int resource, ArrayList<MenuItem> data) {
        super(context, resource, data);
        this.layoutResourceId = resource;
        this.context = context;
        this.data = data;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        MenuItemHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            row.setBackgroundColor(0xFFFFFFFF);
            holder = new MenuItemHolder();
            holder.textItemName = (TextView) row.findViewById(R.id.textView1);
            holder.textQuantity = (TextView) row.findViewById(R.id.textView2);
            holder.textModifiers = (TextView) row.findViewById(R.id.textModifiers);
            holder.btnAdd = (Button) row.findViewById(R.id.button1);
            holder.btnAdd.setTextColor(0xFF33b5e5);
            holder.btnRemove = (Button) row.findViewById(R.id.button2);
            holder.btnRemove.setTextColor(0xFF33b5e5);
            row.setTag(holder);
        } else {
            holder = (MenuItemHolder) row.getTag();
        }
        MenuItem item = data.get(position);
        holder.textItemName.setText(item.getName());
        holder.textQuantity.setText("x" + item.getQuantity());
        holder.textModifiers.setText(item.getModifiers());

        final MenuItem curItem = item;
        final MenuItemHolder curHolder = holder;

        holder.btnAdd.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                String curNum = curItem.getQuantity();
                int num = Integer.parseInt(curNum) + 1;
                curItem.setQuantity(Integer.toString(num));
                curHolder.textQuantity.setText("x" + curItem.getQuantity());

                String total = (String) FragmentItemsSelected.price.getText();
                total = total.replace("TOTAL: €", "");

                DataBaseHelperImpl db_u = new DataBaseHelperImpl(context);

                String detail_qty = db_u.getRecord("select detail_qty from detail\n" +
                        "where _id = '" + curItem.getItem_id() + "'", "detail_qty");

                String detail_price = db_u.getRecord("select detail_price from detail\n" +
                        "where _id = '" + curItem.getItem_id() + "'", "detail_price");

                total = String.format("%.02f", Float.valueOf(total) + Float.valueOf(detail_price));

                FragmentItemsSelected.price.setText("TOTAL: €" + total);

                notifyDataSetChanged();

                List<String> col = new ArrayList<>();
                List<String> values = new ArrayList<>();

                col.add("detail_qty");
                col.add("detail_total");
                col.add("detail_net");

                String qty = String.valueOf(Integer.valueOf(detail_qty) + 1);
                String total_price = String.valueOf(Float.valueOf(detail_price) * (Integer.valueOf(qty)));

                values.add(qty);
                values.add(total_price);
                values.add(total_price);

                db_u.updateRecords("detail", col, values, curItem.getItem_id());

                db_u.close();
            }
        });
        holder.btnRemove.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Log.i("Remove Button Clicked", "**********");
                Toast.makeText(context, "Remove button Clicked " + position,
                        Toast.LENGTH_LONG).show();

                if ((curItem.getQuantity().equals("1")) || curItem.getQuantity().equals("1")) {
                    data.remove(position);

                    String total = (String) FragmentItemsSelected.price.getText();
                    total = total.replace("TOTAL: €", "");

                    DataBaseHelperImpl db_d = new DataBaseHelperImpl(context);

                    String detail_price = db_d.getRecord("select detail_price from detail\n" +
                            "where _id = '" + curItem.getItem_id() + "'", "detail_price");

                    total = String.format("%.02f", Float.valueOf(total) - Float.valueOf(detail_price));

                    FragmentItemsSelected.price.setText("TOTAL: €" + total);

                    notifyDataSetChanged();

                    db_d.deleteRecord("detail", curItem.getItem_id());

                    if (!curItem.getModifiers().isEmpty()) {
                        db_d.deleteModifiers("detail_modifier", curItem.getItem_id());
                    }

                    db_d.close();
                } else {
                    String curNum = curItem.getQuantity();
                    int num = Integer.parseInt(curNum) - 1;
                    curItem.setQuantity(Integer.toString(num));
                    curHolder.textQuantity.setText("x" + curItem.getQuantity());

                    String total = (String) FragmentItemsSelected.price.getText();
                    total = total.replace("TOTAL: €", "");

                    DataBaseHelperImpl db_u = new DataBaseHelperImpl(context);

                    String detail_qty = db_u.getRecord("select detail_qty from detail\n" +
                            "where _id = '" + curItem.getItem_id() + "'", "detail_qty");

                    String detail_price = db_u.getRecord("select detail_price from detail\n" +
                            "where _id = '" + curItem.getItem_id() + "'", "detail_price");

                    total = String.format("%.02f", Float.valueOf(total) - Float.valueOf(detail_price));

                    FragmentItemsSelected.price.setText("TOTAL: €" + total);

                    notifyDataSetChanged();

                    List<String> col = new ArrayList<>();
                    List<String> values = new ArrayList<>();

                    col.add("detail_qty");
                    col.add("detail_total");
                    col.add("detail_net");

                    String qty = String.valueOf(Integer.valueOf(detail_qty) - 1);
                    String total_price = String.valueOf(Float.valueOf(detail_price) * (Integer.valueOf(qty)));

                    values.add(qty);
                    values.add(total_price);
                    values.add(total_price);

                    db_u.updateRecords("detail", col, values, curItem.getItem_id());

                    db_u.close();
                }
            }
        });
        return row;

    }

    static class MenuItemHolder {
        TextView textItemName;
        TextView textQuantity;
        TextView textModifiers;
        Button btnAdd;
        Button btnRemove;
    }
}
