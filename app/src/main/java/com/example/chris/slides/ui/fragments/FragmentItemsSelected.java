package com.example.chris.slides.ui.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.chris.slides.R;
import com.example.chris.slides.adapters.MenuItemCustomAdapter;
import com.example.chris.slides.dao.DataBaseHelperImpl;
import com.example.chris.slides.models.MenuItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class FragmentItemsSelected extends Fragment implements OnItemClickListener, View.OnClickListener {

    private MenuItemCustomAdapter itemAdapter;
    private ArrayList<MenuItem> itemArray = new ArrayList<>();
    private String table_id = "";
    private String order_id = "";
    private Button tableButton;
    private ImageButton cancelButton;
    private ImageButton clearButton;
    private ImageButton sendButton;
    public static TextView price;

    public FragmentItemsSelected() {
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();

        String actionBarTitle = (String) getActivity().getActionBar().getTitle();
        String tableName = actionBarTitle.replace("Orders for ", "");

        if (table_id.isEmpty()) {
            DataBaseHelperImpl db = new DataBaseHelperImpl(getActivity());

            table_id = db.getRecord("select _id as tables_id from tables\n" +
                    "where tables_name = '" + tableName + "'", "tables_id");

            db.close();
        }


        if (order_id.isEmpty()) {
            DataBaseHelperImpl db = new DataBaseHelperImpl(getActivity());

            order_id = db.getRecord("select orders._id as orders_id from orders\n" +
                    "where orders.orders_tables_fk = '" + table_id + "' and orders.orders_status_fk = '5'", "orders_id");

            db.close();
        }


        if (order_id.isEmpty()) {
            itemArray.clear();
            tableButton.setText("No Items Selected");
            sendButton.setEnabled(false);
            sendButton.setVisibility(View.GONE);
            cancelButton.setEnabled(false);
            clearButton.setEnabled(false);
            price.setVisibility(View.GONE);

            itemAdapter.notifyDataSetChanged();
        } else {
            itemArray.clear();
            tableButton.setText("Ordered Items");
            sendButton.setEnabled(true);
            sendButton.setVisibility(View.VISIBLE);
            cancelButton.setEnabled(true);
            clearButton.setEnabled(true);
            price.setVisibility(View.VISIBLE);

            DataBaseHelperImpl db = new DataBaseHelperImpl(getActivity());

            List<HashMap<String, String>> mapList = new ArrayList<>();


            mapList.addAll(db.getListHashRecords("select detail._id as detail_id, detail.detail_desc, detail.detail_qty, detail.detail_total, detail_modifier._id as detail_modifier_id, detail_modifier.detail_modifier_price from detail\n" +
                    "left join detail_modifier on detail._id = detail_modifier.detail_modifier_detail_fk\n" +
                    "where detail.detail_orders_fk = " + order_id + "\n" +
                    "group by detail._id\n" +
                    "order by detail._id"));

            db.close();


            String item_id = "";
            String item_name = "";
            String item_price = "";
            String item_qty = "";
            String detail_modifier_id = "";
            String modifiers = "";
            float total_price = 0;


            for (HashMap<String, String> map : mapList) {
                for (Map.Entry<String, String> entry : map.entrySet()) {

                    if (entry.getKey().equals("detail_desc")) {
                        item_name = entry.getValue();
                    }

                    if (entry.getKey().equals("detail_total")) {
                        item_price = entry.getValue();
                        total_price = total_price + Float.parseFloat(item_price);
                    }

                    if (entry.getKey().equals("detail_id")) {
                        item_id = entry.getValue();
                    }

                    if (entry.getKey().equals("detail_qty")) {
                        item_qty = entry.getValue();
                    }

                    if (entry.getKey().equals("detail_modifier_id")) {
                        detail_modifier_id = entry.getValue();
                    }
                }

                if (detail_modifier_id != null && !detail_modifier_id.isEmpty()) {
                    DataBaseHelperImpl db_m = new DataBaseHelperImpl(getActivity());

                    modifiers = db_m.getRecords("select detail_modifier_name from detail_modifier\n" +
                            "where detail_modifier_detail_fk = '" + item_id + "'", "detail_modifier_name");

                    db_m.close();
                } else {
                    modifiers = "";
                }

                itemArray.add(new MenuItem(item_id, item_name, item_qty, item_price, modifiers));


                price.setGravity(Gravity.END);
                price.setPadding(0, 10, 30, 10);
                price.setText("TOTAL: €" + String.format("%.02f", total_price));


            }


            itemAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ListView userList;

        //Get device resolution
        WindowManager wm = (WindowManager) getActivity().getApplication().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        View rootView = inflater.inflate(R.layout.items_selected, container, false);

        tableButton = (Button) rootView.findViewById(R.id.listTitle);
        tableButton.setText("Ordered Items");
        tableButton.setEnabled(false);
        tableButton.setTypeface(null, Typeface.BOLD);
        tableButton.setTextColor(0xFFFFFFFF);
        tableButton.setBackgroundColor(0xFF33b5e5);

        cancelButton = (ImageButton) rootView.findViewById(R.id.cancelButton);
        cancelButton.setBackgroundColor(0xFF33b5e5);
        cancelButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {


                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                // Yes button clicked
                                itemArray.clear();
                                tableButton.setText("No Items Selected");
                                sendButton.setEnabled(false);
                                sendButton.setVisibility(View.GONE);
                                cancelButton.setEnabled(false);
                                clearButton.setEnabled(false);
                                price.setVisibility(View.GONE);

                                DataBaseHelperImpl db_d = new DataBaseHelperImpl(getActivity());

                                itemAdapter.notifyDataSetChanged();

                                //delete modifiers first
                                String qry = "DELETE FROM detail_modifier\n" +
                                        "WHERE detail_modifier.detail_modifier_detail_fk in (\n" +
                                        "    SELECT detail._id \n" +
                                        "    FROM detail \n" +
                                        "    JOIN detail_modifier ON (detail_modifier_detail_fk = detail._id) \n" +
                                        "    WHERE detail_orders_fk = '" + order_id + "'\n" +
                                        ")";

                                db_d.executeRawQuery(qry);

                                //delete items
                                db_d.deleteRecords("detail", "detail_orders_fk = " + order_id);

                                db_d.updateRecords("orders", "orders_status_fk", "2", order_id);

                                db_d.close();

                                Toast.makeText(getActivity(),
                                        "Canceled!", Toast.LENGTH_SHORT).show();
                                break;
                            case DialogInterface.BUTTON_NEGATIVE:
                                // No button clicked
                                // do nothing
                                break;
                        }
                    }
                };
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Cancel Order?").setPositiveButton("Yes", dialogClickListener).setNegativeButton("No", dialogClickListener).show();

            }

        });


        clearButton = (ImageButton) rootView.findViewById(R.id.clearButton);

        clearButton.setBackgroundColor(0xFF33b5e5);
        clearButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                // Yes button clicked
                                itemArray.clear();
                                tableButton.setText("No Items Selected");
                                sendButton.setEnabled(false);
                                sendButton.setVisibility(View.GONE);
                                cancelButton.setEnabled(false);
                                clearButton.setEnabled(false);
                                price.setVisibility(View.GONE);

                                DataBaseHelperImpl db_d = new DataBaseHelperImpl(getActivity());

                                itemAdapter.notifyDataSetChanged();

                                //delete modifiers first
                                String qry = "DELETE FROM detail_modifier\n" +
                                        "WHERE detail_modifier.detail_modifier_detail_fk in (\n" +
                                        "    SELECT detail._id \n" +
                                        "    FROM detail \n" +
                                        "    JOIN detail_modifier ON (detail_modifier_detail_fk = detail._id) \n" +
                                        "    WHERE detail_orders_fk = '" + order_id + "'\n" +
                                        ")";

                                db_d.executeRawQuery(qry);

                                //delete items
                                db_d.deleteRecords("detail", "detail_orders_fk = " + order_id);

                                db_d.close();

                                Toast.makeText(getActivity(),
                                        "Cleared!", Toast.LENGTH_SHORT).show();
                                break;
                            case DialogInterface.BUTTON_NEGATIVE:
                                // No button clicked
                                // do nothing
                                break;
                        }
                    }
                };
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Clear Order?").setPositiveButton("Yes", dialogClickListener).setNegativeButton("No", dialogClickListener).show();

            }

        });

        //Send button footer
        sendButton = new ImageButton(getActivity());
        sendButton.setImageResource(R.drawable.ic_action_send);
        sendButton.setBackgroundColor(0xFFFFFFFF);
        sendButton.setOnClickListener(this);


        /**
         * set item into adapter
         */
        itemAdapter = new MenuItemCustomAdapter(getActivity(), R.layout.row,
                itemArray);
        userList = (ListView) rootView.findViewById(R.id.listView);

        price = new TextView(getActivity());
        price.setTypeface(null, Typeface.BOLD);
        price.setTextColor(0xFFFFFFFF);
        price.setBackgroundColor(0xFF33b5e5);

        userList.addFooterView(price);
        userList.addFooterView(sendButton);
        userList.setItemsCanFocus(false);
        userList.setAdapter(itemAdapter);
        userList.setBackgroundColor(0x00FFFFFF);

        return rootView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.i("List View Clicked", "**********");
        Toast.makeText(getActivity(),
                "List View Clicked:" + position, Toast.LENGTH_LONG)
                .show();
    }


    // Have to implement with the OnClickListner
    // onClick is called when a view has been clicked.
    @Override
    public void onClick(View view) {

        Toast.makeText(getActivity(),
                "Sent", Toast.LENGTH_LONG)
                .show();
        //Navigate back after saving to DB
        NavUtils.navigateUpFromSameTask(getActivity());
    }
}
