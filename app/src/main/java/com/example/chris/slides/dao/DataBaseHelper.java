package com.example.chris.slides.dao;

import android.database.SQLException;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public interface DataBaseHelper {

    void createDataBase() throws IOException;

    void openDataBase() throws SQLException;

    List<String> getListRecords(String query, String column);

    String getRecord(String query, String column);

    String getRecords(String query, String column);

    List<HashMap<String, String>> getListHashRecords(String query);

    long insertRecords(String table, List<String> column, List<String> value);

    long updateRecords(String table, List<String> column, List<String> value, String id);

    long updateRecords(String table, String column, String value, String id);

    boolean deleteRecord(String table, String id);

    boolean deleteModifiers(String table, String id);

    boolean deleteRecords(String table, String id);

    void executeRawQuery(String query);
}
