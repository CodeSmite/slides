package com.example.chris.slides.ui.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.chris.slides.R;
import com.example.chris.slides.adapters.MenuItemPriceCustomAdapter;
import com.example.chris.slides.dao.DataBaseHelper;
import com.example.chris.slides.dao.DataBaseHelperImpl;
import com.example.chris.slides.models.Item;
import com.example.chris.slides.models.MenuItemPrice;
import com.example.chris.slides.models.SectionMenuCategory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class FragmentSelectMainDish extends Fragment {

    private OnMenuItemSelectedListener mCallback;

    // Container Activity must implement this interface
    public interface OnMenuItemSelectedListener {
        void onItemSelected(int position);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mCallback = (OnMenuItemSelectedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnMenuItemSelectedListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ListView userList;
        MenuItemPriceCustomAdapter itemAdapter;
        List<Item> itemArray = new ArrayList<>();

        View rootView = inflater.inflate(R.layout.select_main_dish, container, false);

        /**
         * add item in arraylist
         */

        DataBaseHelper db = new DataBaseHelperImpl(getActivity());

        List<HashMap<String, String>> mapList = new ArrayList<>();


        mapList.addAll(db.getListHashRecords("select item._id as item_id, item.item_name, item.item_price, item.item_configure, category._id as category_id, category.category_name from item\n" +
                "join category on item_category_fk = category._id\n" +
                "join main_category on category.category_main_category_fk = main_category._id\n" +
                "where main_category._id = 1\n" +
                "order by category._id"));

        //db.close();

        String item_id = "";
        String item_name = "";
        String item_price = "";
        String item_configure = "";
        String category_id = "";
        String prv_category_id = "";
        String category_name = "";


        for (HashMap<String, String> map : mapList) {
            for (Map.Entry<String, String> entry : map.entrySet()) {

                if (entry.getKey().equals("category_id")) {
                    prv_category_id = category_id;
                    category_id = entry.getValue();
                }

                if (entry.getKey().equals("category_name")) {
                    category_name = entry.getValue();
                }

                if (entry.getKey().equals("item_name")) {
                    item_name = entry.getValue();
                }

                if (entry.getKey().equals("item_price")) {
                    item_price = entry.getValue();
                }

                if (entry.getKey().equals("item_id")) {
                    item_id = entry.getValue();
                }

                if (entry.getKey().equals("item_configure")) {
                    item_configure = entry.getValue();
                }
            }

            if (!(prv_category_id.equals(category_id))) {
                itemArray.add(new SectionMenuCategory(category_name, category_id));
            }

            itemArray.add(new MenuItemPrice(item_id, item_name, item_price, item_configure, category_id));

        }

        /**
         * set item into adapter
         */
        itemAdapter = new MenuItemPriceCustomAdapter(getActivity(), R.layout.menu_row,
                itemArray);
        userList = (ListView) rootView.findViewById(R.id.listView);
        userList.setItemsCanFocus(false);

        userList.setAdapter(itemAdapter);
        userList.setBackgroundColor(Color.WHITE);
        userList.setClickable(false);

        return rootView;
    }

}
